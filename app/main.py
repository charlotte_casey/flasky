from flask import Flask, jsonify, request
import os


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__)) # __file__ is the file i'm currently in
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'kubrick.db')


# home endpoint
@app.route("/")
def home():
    return jsonify(data = "Welcome :)")

if __name__ == "__main__":
    app.run(host = '127.0.0.1', port = 5000)